package com.technaxis.querydsl.repositories.custom.helpers;

import com.querydsl.core.types.Predicate;
import com.technaxis.querydsl.dto.forms.UsersSearchForm;
import com.technaxis.querydsl.model.QUser;

import java.util.ArrayList;
import java.util.List;

/**
 * 08.10.2019
 *
 * @author Dinar Rafikov
 * @version 1.0
 */
public class UserRepositoryHelper extends RepositoryHelper {

    public static Predicate[] searchUsersPredicates(UsersSearchForm form) {
        QUser user = QUser.user;
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(user.deleted.isFalse());
        if (form.getRole() != null) {
            predicates.add(user.role.eq(form.getRole()));
        }
        final String formattedQuery = form.getFormattedQuery();
        if (formattedQuery == null || formattedQuery.isEmpty()) {
            predicates.add(user.name.like(formattedQuery).or(user.email.like(formattedQuery)));
        }
        return predicates.toArray(new Predicate[0]);
    }
}
