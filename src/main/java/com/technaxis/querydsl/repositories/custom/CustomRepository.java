package com.technaxis.querydsl.repositories.custom;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.jpa.impl.JPAQuery;
import com.technaxis.querydsl.model.enums.sorting.ISortType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Nullable;

/**
 * @author Dmitry Sadchikov
 */
public interface CustomRepository {

    default <E> Page<E> pageBy(JPAQuery<E> jpaQuery) {
        return pageBy(jpaQuery.fetchResults());
    }

    default <E> Page<E> pageBy(QueryResults<E> queryResults) {
        PageRequest pageRequest = PageRequest.of((int) queryResults.getOffset(), (int) queryResults.getLimit());
        return new PageImpl<>(queryResults.getResults(), pageRequest, queryResults.getTotal());
    }

    default OrderSpecifier[] getOrders(@Nullable ISortType sortType, @Nullable Order order) {
        if (sortType == null) {
            return new OrderSpecifier[0];
        }
        if (order == null) {
            order = Order.ASC;
        }
        return sortType.of(order);
    }
}
